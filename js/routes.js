// @flow
import React, { Component } from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import * as Components from "./components";

type Props = {};
type State = {};

export class RootNavigator extends Component<Props, State> {
  render() {
    const Stack = createStackNavigator();

    return (
      <NavigationContainer>
        <Stack.Navigator initialRouteName="Main">
          <Stack.Screen
            name="Main"
            component={Components.RecipesListScreen}
            options={{ title: "Recipe puppy search" }}
          />
        </Stack.Navigator>
      </NavigationContainer>
    );
  }
}
