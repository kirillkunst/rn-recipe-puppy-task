// @flow
import React, { Component } from "react";
import type { RecipeItem } from "../types";
import { View, FlatList } from "react-native";
import { ListItem, Left, Body, Thumbnail, Text } from "native-base";

type Props = {
  results: Array<RecipeItem>
};

type State = {};

export default class SearchResults extends Component<Props, State> {
  renderItem(item: RecipeItem, index: number) {
    return (
      <ListItem avatar key={index}>
        <Left>
          <Thumbnail small source={{ uri: item.thumbnail }} />
        </Left>
        <Body>
          <Text>{item.title}</Text>
        </Body>
      </ListItem>
    );
  }

  render() {
    const { results } = this.props;
    return (
      <View>
        <FlatList
          data={results}
          keyExtractor={item => item.title}
          renderItem={({ item, index }: { item: RecipeItem, index: number }) =>
            this.renderItem(item, index)
          }
        />
      </View>
    );
  }
}
