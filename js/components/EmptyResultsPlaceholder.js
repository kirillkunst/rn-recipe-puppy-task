// @flow
import React, { Component } from "react";
import { View, Text } from "react-native";
import Style from "../stylesheets/style";

type Props = {};
type State = {};

export default class EmptyResultsPlaceholder extends Component<Props, State> {
  render() {
    return (
      <View style={Style.placeholderContainer}>
        <Text style={Style.placeholderText}>
          We're sorry but we have no results for you
        </Text>
      </View>
    );
  }
}
