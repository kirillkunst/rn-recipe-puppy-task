// @flow
import React, { Component } from "react";
import { TouchableWithoutFeedback, Keyboard } from "react-native";

type Props = {
  children?: React$Element<any>
};
type State = {};

export default class DismissKeyboardView extends Component<Props, State> {
  render() {
    return (
      <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
        {this.props.children}
      </TouchableWithoutFeedback>
    );
  }
}
