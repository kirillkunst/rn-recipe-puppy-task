// @flow
import React, { Component } from "react";
import { Header, Item, Input, Icon } from "native-base";
import { View } from "react-native";
import { debounce } from "lodash";

type Props = {
  onValueChanged: (value: string) => void
};
type State = {};

export default class SearchBar extends Component<Props, State> {
  handleInputThrottled: any;

  constructor(props: Props) {
    super(props);
    this.handleInputThrottled = debounce(this.handleInput, 300);
  }

  handleInput = (data: string) => {
    const { onValueChanged } = this.props;
    onValueChanged(data);
  };

  render() {
    return (
      <View>
        <Header searchBar rounded>
          <Item>
            <Icon name="ios-search" />
            <Input
              placeholder="Search"
              onChangeText={this.handleInputThrottled}
            />
          </Item>
        </Header>
      </View>
    );
  }
}
