// @flow
import React, { Component } from "react";

import type { GlobalState, GlobalProps } from "../types";
import { connect, compose } from "../redux";

import { Container, Spinner } from "native-base";

import Style from "../stylesheets/style";

import SearchBar from "./SearchBar";
import SearchResults from "./SearchResults";
import EmptyResultsPlaceholder from "./EmptyResultsPlaceholder";
import DismissKeyboardView from "./DismissKeyboardView";

import * as ReduxActions from "../actions";

type MapStateToProps = {
  recipes: $PropertyType<GlobalState, "recipes">
};

type Props = GlobalProps & MapStateToProps;
type State = {};

class RecipesListScreen extends Component<Props, State> {
  state = {};

  componentDidMount() {
    this.props.dispatch(ReduxActions.recipes.searchRecipes(""));
  }

  onValueChanged = (query: string) => {
    this.props.dispatch(ReduxActions.recipes.searchRecipes(query));
  };

  render() {
    const { recipes } = this.props;

    return (
      <DismissKeyboardView>
        <Container style={Style.contentContainer}>
          <SearchBar onValueChanged={this.onValueChanged} />
          {recipes.loadingInProgress && <Spinner color="black" />}
          {recipes.result && !recipes.loadingInProgress && (
            <SearchResults results={recipes.result.results} />
          )}
          {(recipes.result.results == null ||
            recipes.result.results.length == 0) &&
            !recipes.loadingInProgress && <EmptyResultsPlaceholder />}
        </Container>
      </DismissKeyboardView>
    );
  }
}

const mapStateToProps = (state: GlobalState): MapStateToProps => {
  return {
    recipes: state.recipes
  };
};

export default compose(connect(mapStateToProps))(RecipesListScreen);
