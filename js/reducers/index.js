// @flow
import recipes from "./recipes";

const appReducers = {
  recipes
  // Place your other reducers here
};

export default appReducers;
