// @flow
import type { GlobalState } from "../types";
import type { RecipeAction } from "../actions/recipes";

type InitialState = $PropertyType<GlobalState, "recipes">;

const initialState: Exact<InitialState> = {
  recipes: {},
  loadingInProgress: false
};

export default function recipes(
  state: ?InitialState = null,
  action: RecipeAction
): Exact<InitialState> {
  if (state == null) {
    state = initialState;
  }

  switch (action.type) {
    case "SET_RECIPES_SEARCH_RESULT":
      return {
        ...state,
        result: action.result,
        loadingInProgress: false
      };
    case "SET_RECIPES_LOADING":
      return {
        ...state,
        loadingInProgress: true
      };
    default:
      (action: empty);
      return state;
  }
}
