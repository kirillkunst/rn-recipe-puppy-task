// @flow

const API_URL = "http://www.recipepuppy.com/api/";
const headers = {
  Accept: "application/json",
  "Content-Type": "application/json"
};

export const fetchRecipes = async (query: string) => {
  let url = API_URL + "?q=" + query;

  const response = await fetch(url, {
    method: "GET",
    headers: headers
  });

  const json = response.json();

  return json;
};
