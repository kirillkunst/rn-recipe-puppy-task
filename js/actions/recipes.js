// @flow
import type { Dispatch, RecipesResult } from "../types";
import { fetchRecipes } from "../api";

export type RecipeAction =
  | {
      type: "SET_RECIPES_SEARCH_RESULT",
      result: RecipesResult
    }
  | { type: "SET_RECIPES_LOADING" };

export const searchRecipes = (query: string) => {
  return async (dispatch: Dispatch) => {
    console.log("start fetch...");
    dispatch({ type: "SET_RECIPES_LOADING" });

    const result = await fetchRecipes(query);
    dispatch({
      type: "SET_RECIPES_SEARCH_RESULT",
      result: result
    });
  };
};
