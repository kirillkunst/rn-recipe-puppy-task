// @flow
import * as recipes from "./recipes";

import type { RecipeAction } from "./recipes";

export type Action =
  | { type: "ERROR", error: Error }
  | RecipeAction;

export { recipes };
