// @flow

import type { Dispatch, GetState, GlobalState } from "./types";
import type { Action } from "./actions";

import AsyncStorage from "@react-native-community/async-storage";
import appReducers from "./reducers";
import { persistStore, persistReducer } from "redux-persist";

import { createStore, combineReducers, applyMiddleware, compose } from "redux";
import thunk from "redux-thunk";

export const persistConfig = {
  key: "root",
  storage: AsyncStorage,
  whitelist: ["recipes"],
  timeout: 60 * 1000
};

export const buildRootReducer = (reducers: any, persistConfig: any) => {
  const combinedReducers = combineReducers(reducers);

  const rootReducer = (state: GetState, action: Action) => {
    if (action.type == "CLEAR_STATE") {
      let cleanState = combinedReducers({}, {});
      state = {
        ...cleanState
      };
    }
    return combinedReducers(state, action);
  };

  const persistedReducer = persistReducer<any, any>(persistConfig, rootReducer);

  return persistedReducer;
};

const rootReducer = buildRootReducer(appReducers, persistConfig);

export type Store = {
  getState: GetState,
  dispatch: Dispatch,
  subscribe: Function
};

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export let store: Store = createStore(
  rootReducer,
  composeEnhancers(applyMiddleware(thunk))
);

export let storePersistor = persistStore(store);

export let waitForStoreReady = async () => {
  await new Promise((resolve: any) => {
    let _unsubscribe;

    const handlePersistorState = () => {
      let { bootstrapped } = storePersistor.getState();
      if (bootstrapped) {
        resolve();
        _unsubscribe && _unsubscribe();
      }
    };

    _unsubscribe = storePersistor.subscribe(handlePersistorState);
    handlePersistorState();
  });
};

export function observeStore<R>(
  selectFn: (state: GlobalState) => R,
  onChange: (value: R, state: GlobalState) => void,
  changeOnly?: boolean
) {
  let currentState;
  let firstRun = true;

  function handleChange() {
    let state = store.getState();
    let nextState = selectFn(state);
    if (nextState !== currentState) {
      currentState = nextState;
      if (!changeOnly || !firstRun) {
        onChange(currentState, state);
      }

      firstRun = false;
    }
  }

  let unsubscribe = store.subscribe(handleChange);
  handleChange();
  return unsubscribe;
}
