// @flow

import type { Action } from "./actions";
export type { Action };

export type GlobalProps = {
  navigation: Object,
  screenProps: Object,
  dispatch: Function
};

export type _GlobalState = {
  recipes: {
    loadingInProgress: boolean,
    result: RecipesResult
  }
};

export type GlobalState = $Exact<_GlobalState>;

export type Dispatch = (action: Action | ThunkAction | PromiseAction) => any;
export type GetState = () => GlobalState;
export type ThunkAction = (dispatch: Dispatch, getState: GetState) => any;
export type PromiseAction = Promise<Action>;

export type RecipesResult = {
  title: string,
  version: string,
  results: Array<Object>
};

export type RecipeItem = {
  title: string,
  href: string,
  ingredients: string,
  thumbnail: string
};
