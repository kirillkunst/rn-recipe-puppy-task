// @flow
import { StyleSheet } from "react-native";

export default StyleSheet.create({
  contentContainer: {
    flex: 1,
    width: "100%",
    justifyContent: "flex-start"
  },
  placeholderContainer: {
    flex: 1,
    width: "100%",
    justifyContent: "flex-start",
    alignItems: "center"
  },
  placeholderText: {
    marginHorizontal: 40,
    marginTop: 100,
    fontSize: 20,
    textAlign: "center"
  }
});
