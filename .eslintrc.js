module.exports = {
  env: {
    browser: true,
    es6: true,
    jest: true
  },
  extends: ["eslint:recommended", "plugin:flowtype/recommended"],
  globals: {
    Atomics: "readonly",
    SharedArrayBuffer: "readonly"
  },
  parserOptions: {
    ecmaFeatures: {
      jsx: true
    },
    ecmaVersion: 2018,
    sourceType: "module"
  },
  parser: "babel-eslint",
  plugins: ["react", "flowtype"],
  rules: {
    "no-unused-vars": 1,
    "flowtype/boolean-style": [2, "boolean"],
    "flowtype/define-flow-type": 1,
    "flowtype/delimiter-dangle": [2, "never"],
    "flowtype/generic-spacing": [2, "never"],
    "flowtype/no-mixed": 2,
    "flowtype/no-primitive-constructor-types": 2,
    "flowtype/no-types-missing-file-annotation": 2,
    "flowtype/no-weak-types": 0,
    "flowtype/object-type-delimiter": [2, "comma"],
    "flowtype/require-parameter-type": 1,
    "flowtype/require-readonly-react-props": 0,
    "flowtype/require-return-type": [
      0,
      "always",
      {
        annotateUndefined: "never"
      }
    ],
    "flowtype/require-valid-file-annotation": 2,
    "flowtype/semi": [2, "always"],
    "flowtype/space-after-type-colon": [1, "always"],
    "flowtype/space-before-generic-bracket": [1, "never"],
    "flowtype/space-before-type-colon": [1, "never"],
    "flowtype/type-id-match": [0, "^([A-Z][a-z0-9]+)+Type$"],
    "flowtype/union-intersection-spacing": [1, "always"],
    "flowtype/use-flow-type": 1,
    "flowtype/valid-syntax": 1,
    "react/jsx-uses-react": 1,
    "react/jsx-uses-vars": 2,
    "no-console": "off"
  },
  globals: {
    __DEV__: true,
    require: true,
    __dirname: true,
    module: true
  },
  settings: {
    flowtype: {
      onlyFilesWithFlowAnnotation: false
    }
  }
};
