### Recipe puppy search app

Packages and technologies:

- `ES`
- `Flow` - to support strong types
- `Prettier` - to make the code be nice
- `ESLint` - to reduce stupid errors
- `Lodash` - to support `throttle` for search input
- `NativeBase` - to make the app looks pretty
- `Redux` - to make clean state management for the app

Some useful info before code review:

- All JS code is inside `js` folder
- `api` folder is for API services
- `actions` for Redux actions
- `reducers` for Redux reducers
- `components` - All UI screens and components are there
- `stylesheets` - I thought about to place some useful stylesheets there but there are not so much code in fact
- `routes.js` - I placed routing configuration here to make it configurable and extandable in the future
- `types.js` - I placed all common types there like `Recipe item` type or some types for Redux implementation
- I took care about keyboard dismissing for search input and created `DismissKeyboardView` for that
- I made some support to save current state of the app but it's only for future purposes.

The project completely ready to develop next features and it contains almost all necessary stuff as for me.

Some omissions:

- No tests
- No localization
- No proper error handling
