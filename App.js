/**
 * @format
 * @flow
 */
import type { Dispatch } from "./js/types";
import { Provider } from "react-redux";
import { PersistGate } from "redux-persist/integration/react";
import { RootNavigator } from "./js/routes";
import React, { Component, Fragment } from "react";
import { connect, storePersistor, store } from "./js/redux";

type AppProps = {
  dispatch: Dispatch,
  nav: Object
};

type AppState = {};

class App extends Component<AppProps, AppState> {
  render() {
    return (
      <Fragment>
        <RootNavigator />
      </Fragment>
    );
  }
}

const AppWithDispatch = connect(() => ({}))(App);

type RootProps = {};

class Root extends Component<RootProps> {
  render() {
    return (
      <Provider store={store}>
        <PersistGate loading={null} persistor={storePersistor}>
          <AppWithDispatch />
        </PersistGate>
      </Provider>
    );
  }
}

export default Root;
